# Project name
PROJ_NAME=wav_audio
OUTPATH=build

###################################################

# Check for valid float argument
# NOTE that you have to run make clan after
# changing these as hardfloat and softfloat are not
# binary compatible
ifneq ($(FLOAT_TYPE), hard)
ifneq ($(FLOAT_TYPE), soft)
#override FLOAT_TYPE = hard
override FLOAT_TYPE = soft
endif
endif

###################################################

CROSS_COMPILE=arm-none-eabi-
CC=$(CROSS_COMPILE)gcc
AR=$(CROSS_COMPILE)ar
SIZE=$(CROSS_COMPILE)size
OBJCOPY=$(CROSS_COMPILE)objcopy
AS=$(CROSS_COMPILE)gcc -x assembler-with-cpp

CFLAGS = -std=gnu99 -g -O2 -Wall -Tstm32_flash.ld
CFLAGS += -mlittle-endian -mthumb -mthumb-interwork -nostartfiles -mcpu=cortex-m4

ifeq ($(FLOAT_TYPE), hard)
CFLAGS += -fsingle-precision-constant -Wdouble-promotion
CFLAGS += -mfpu=fpv4-sp-d16 -mfloat-abi=hard
else
CFLAGS += -msoft-float
endif

CFLAGS += -DSTM32F40_41xxx -DUSE_STDPERIPH_DRIVER
CFLAGS += -DMEDIA_USB_KEY

###################################################

#vpath %.c src
#vpath %.a lib
vpath %.o $(OUTPATH)

ROOT=$(shell pwd)
INCLUDES = -I$(ROOT)/include
INCLUDES += -I$(ROOT)/include/lib

#CMSIS
CMSISDIR = libs/CMSIS
CMSISSRCDIR = $(CMSISDIR)/src
CMSISINCDIR = $(ROOT)/$(CMSISDIR)/include
STM32F4_LIBRARYS += $(wildcard $(CMSISSRCDIR)/*.c)
INCLUDES += -I$(CMSISINCDIR)

#misc
MISCDIR = libs/misc
MISCSRCDIR = $(MISCDIR)/src
MISCINCDIR = $(ROOT)/$(MISCDIR)/include
STM32F4_LIBRARYS += $(wildcard $(MISCSRCDIR)/*.c)
INCLUDES += -I$(MISCINCDIR)

#StdPeriph
STDPERIPHDIR = libs/STM32F4xx_StdPeriph_Driver
STDPERIPHSRCDIR = $(STDPERIPHDIR)/src
STDPERIPHINCDIR = $(ROOT)/$(STDPERIPHDIR)/inc
STM32F4_LIBRARYS += $(wildcard $(STDPERIPHSRCDIR)/*.c)
INCLUDES += -I$(STDPERIPHINCDIR)

#USB OTG
USBOTGDIR = src/STM32_USB_OTG_Driver
USBOTGSRCDIR = $(USBOTGDIR)/src
USBOTGINCDIR = $(ROOT)/$(USBOTGDIR)/inc
INCLUDES += -I$(USBOTGINCDIR)
STM32F4_LIBRARYS += $(wildcard $(USBOTGSRCDIR)/*.c)

#USB Host Core
USBHOSTDIR = src/STM32_USB_HOST_Library/Core
USBHOSTSRCDIR = $(USBHOSTDIR)/src
USBHOSTINCDIR = $(ROOT)/$(USBHOSTDIR)/inc
INCLUDES += -I$(USBHOSTINCDIR)
STM32F4_LIBRARYS += $(wildcard $(USBHOSTSRCDIR)/*.c)

#USB Host MSC
USBHOSTCLASSMSCDIR = src/STM32_USB_HOST_Library/Class/MSC
USBHOSTCLASSMSCSRCDIR = $(USBHOSTCLASSMSCDIR)/src
USBHOSTCLASSMSCINCDIR = $(ROOT)/$(USBHOSTCLASSMSCDIR)/inc
INCLUDES += -I$(USBHOSTCLASSMSCINCDIR)
STM32F4_LIBRARYS += $(wildcard $(USBHOSTCLASSMSCSRCDIR)/*.c)

#STM32F4-Discovery
STM32F4DISCOVERYDIR = src/STM32F4-Discovery
STM32F4DISCOVERYSRCDIR = $(STM32F4DISCOVERYDIR)
STM32F4DISCOVERYINCDIR = $(ROOT)/$(STM32F4DISCOVERYDIR)
INCLUDES += -I$(STM32F4DISCOVERYINCDIR)
STM32F4_LIBRARYS += $(wildcard $(STM32F4DISCOVERYSRCDIR)/*.c)
LDFLAGS += -L$(STM32F4DISCOVERYINCDIR)
LIBS += -lPDMFilter_GCC

#fat_fs
FSTFSDIR = src/fat_fs
FSTFSSRCDIR = $(FSTFSDIR)/src
FSTFSINCDIR = $(ROOT)/$(FSTFSDIR)/inc
INCLUDES += -I$(FSTFSINCDIR)
STM32F4_LIBRARYS += $(wildcard $(FSTFSSRCDIR)/*.c)

#SRC 
SRCS += $(wildcard src/*.c)

# add startup file to build
SRCS += src/startup_stm32f4xx.S

SRCS += $(STM32F4_LIBRARYS)

__OBJS = $(SRCS:.c=.o)
_OBJS = $(__OBJS:.S=.o)
OBJS = $(patsubst %,$(OUTPATH)/%,$(_OBJS))

CPFLAGS += -MD -MP -MF .dep/$*.d

INCLUDES += -Iinc -Iplatform/STM32F4xx
LDFLAGS += -L$(ROOT)/ldscripts -T libs.ld -T mem.ld -T sections.ld

###################################################
.PHONY: all clean

all: $(OBJS) $(PROJ_NAME).elf  $(PROJ_NAME).hex $(PROJ_NAME).bin
	$(SIZE) $(OUTPATH)/$(PROJ_NAME).elf


$(OUTPATH)/%.o: %.c
	if [ ! -d $(dir $@) ]; then mkdir -p $(dir $@); fi
	if [ ! -d $(dir .dep/$<) ]; then mkdir -p $(dir .dep/$<); fi
	$(CC) -c $(CPFLAGS) $(CFLAGS) $(INCLUDES) -I . $(INCDIR) $< -o $@

$(OUTPATH)/%.o: %.S
	if [ ! -d $(dir $@) ]; then mkdir -p $(dir $@); fi
	$(AS) -c $(ASFLAGS) $< -o $@

%elf: $(OBJS)
	$(CC) $(OBJS) $(LDFLAGS) $(LIBS) -o $(OUTPATH)/$@

%hex: %elf
	$(OBJCOPY) -O ihex $(OUTPATH)/$(PROJ_NAME).elf $(OUTPATH)/$(PROJ_NAME).hex

%bin: %elf
	$(OBJCOPY) -O binary $(OUTPATH)/$(PROJ_NAME).elf $(OUTPATH)/$(PROJ_NAME).bin

clean:
	rm -rf .dep
	rm -rf build


#
# Include the dependency files, should be the last of the makefile
#
#-include $(shell mkdir -p .dep) $(wildcard .dep/*)
-include $(OBJS:.o=.d)

# *** EOF ***
